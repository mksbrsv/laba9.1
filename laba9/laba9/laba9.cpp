
#include "pch.h"
#include <iostream>
#include <stdio.h>
#include <omp.h>
#include <locale.h>

int fibonacci1(int m);
int fibonacci2(int m);

int main()
{
	setlocale(LC_CTYPE, "Russian");
	int sum, sum1, m;
	double start, finish, time, time1;
	printf("Введите число, до которого считать: ");
	scanf_s("%d", &m);
	start = omp_get_wtime();
	sum = fibonacci1(m);
	finish = omp_get_wtime();
	time = finish - start;
	start = omp_get_wtime();
	sum1 = fibonacci2(m);
	finish = omp_get_wtime();
	time1 = finish - start;
	printf("Время работы рекурсивной программы: %lf\nРезультат работы прямой программы: %lf\n", time, time1);
	printf("Результат работы рекурсивной программы: %d\nРезультат работы прямой программы: %d", sum, sum1);

}

int fibonacci1(int m) {
	if (m == 0)
		return 0;
	if (m == 1)
		return 1;
	return fibonacci1(m-1) + fibonacci1(m-2);

}

int fibonacci2(int m) {
	int f1 = 1, f0 = 0;
	while (m != 0) {
		f0 = f0 + f1;
		f1 = f0 - f1;
		m = m - 1;
	}
	return f0;
}